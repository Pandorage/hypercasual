﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class DamageDealer : MonoBehaviour
{
    public Slider enemylifeBar;
    public DamageType actualEnemyType;
    public float maxLife;
    private float actualLife;
    public Image enemySprite;
    public float positiveMultiplier;
    public float negativeMultiplier;
    public float baseDamage;
    public float maxAttackTimer;
    private float actualTimer;
    public Slider timeSlider;
    public Slider playerLifebar;
    public float maxPlayerLife;
    private float actualPlayerLife;
    private int bossPhase;
    private bool attackCanceled;
    public AttackDefenseAnimation anim;

    private void Start()
    {
        ChangeEnemy();
        actualLife = maxLife;
        actualPlayerLife = maxPlayerLife;
    }

    public enum DamageType
    {
        Fire,
        Plant,
        Water
    }

    private void Update()
    {
        actualTimer += Time.deltaTime;
        if (actualTimer > maxAttackTimer)
        {
            actualTimer = maxAttackTimer;
        }
        timeSlider.value = actualTimer / maxAttackTimer;
        if (actualTimer == maxAttackTimer)
        {
            SwitchPhase();
        }
    }

    private void SwitchPhase()
    {
        actualTimer = 0;
        timeSlider.value = 0;
        if (bossPhase == 2 && !attackCanceled)
        {
            AttackPlayer();
        }
        bossPhase++;
        bossPhase = bossPhase % 3;
        switch (bossPhase)
        {
            case 0:
                anim.Attack(false);
                anim.Defense(false);
                break;
            case 1:
                anim.Attack(false);
                anim.Defense(true);
                break;
            case 2:
                attackCanceled = false;
                anim.Attack(true);
                anim.Defense(false);
                break;
        }
        ChangeEnemy();
    }

    public void AttackPlayer()
    {
        actualPlayerLife -= 10;
        if (actualPlayerLife < 0)
        {
            actualPlayerLife = 0;
        }

        playerLifebar.value = actualPlayerLife / maxPlayerLife;
        if (actualPlayerLife == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
    
    public void Hit(string symbol)
    {
        if (bossPhase != 1)
        {
            bool failed = false;
            DamageType type = DamageType.Fire;
            switch (symbol)
            {
                case "P":
                    type = DamageType.Fire;
                    break;
                case "N":
                    type = DamageType.Plant;
                    break;
                case "D":
                    type = DamageType.Water;
                    break;
                default:
                    failed = true;
                    break;
            }

            if (!failed)
            {
                if (type == DamageType.Fire && actualEnemyType == DamageType.Plant
                    || type == DamageType.Plant && actualEnemyType == DamageType.Water
                    || type == DamageType.Water && actualEnemyType == DamageType.Fire)
                {
                    Damage(baseDamage*positiveMultiplier);
                    attackCanceled = true;
                } else if (
                    type == DamageType.Water && actualEnemyType == DamageType.Plant
                    || type == DamageType.Fire && actualEnemyType == DamageType.Water
                    || type == DamageType.Plant && actualEnemyType == DamageType.Fire)
                {
                    Damage(baseDamage*negativeMultiplier);
                }
                else
                {
                    Damage(baseDamage);
                }
                SwitchPhase();
            }
        }
        else
        {
            AttackPlayer();
        }
    }

    private void Damage(float dmg)
    {
        Debug.Log(dmg);
        Debug.Log(actualLife);
        actualLife -= dmg;
        if (actualLife < 0)
        {
            actualLife = 0;
        }
        
        enemylifeBar.value = actualLife / maxLife;
        
        if (actualLife == 0)
        {
            ChangeEnemy();
        }
    }

    private void ChangeEnemy()
    {
        actualTimer = 0;
        timeSlider.value = 0;
        switch (Random.Range(0,3))
        {
            case 0:
                actualEnemyType = DamageType.Fire;
                enemySprite.color = Color.red;
                break;
            case 1:
                actualEnemyType = DamageType.Water;
                enemySprite.color = Color.blue;
                break;
            case 2:
                actualEnemyType = DamageType.Plant;
                enemySprite.color = Color.green;
                break;
        }
    }
}