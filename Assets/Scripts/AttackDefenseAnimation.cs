﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackDefenseAnimation : MonoBehaviour
{
    public GameObject attack;

    public GameObject defense;

    public void Attack(bool isShow)
    {
        
        attack.SetActive(isShow);
    }

    public void Defense(bool isShow)
    {
        defense.SetActive(isShow);
    }
    
    
}
